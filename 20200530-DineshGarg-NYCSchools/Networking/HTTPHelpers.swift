//
//  HTTPHelpers.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

enum Endpoint: String {
    case schools = "s3k6-pzi2.json"
    case scores = "f9bf-2cp4.json"
}

// We can fetch data for some other distrcits too, like Chicago or LA etc.
// keeping that in mind, structuring baseurl as an enum for expansion.
enum BaseURL: String {
    /// If we  use URLComponents, we can even remove https, and may be
    /// add another endpoint to set resource path. But, seems like this can
    /// work for different city school datas as well.
    case cityOfNeywork = "https://data.cityofnewyork.us/resource/"
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    // we are not using any case other than get, so keeping it short.
}
