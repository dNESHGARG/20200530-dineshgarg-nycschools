//
//  SchoolScoreService.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

// Create service request for fetching scores data.
struct SchoolScoreService: Service {
    typealias modelObject = ScoreData

    var session = URLSession.shared
    var httpMethod: HTTPMethod = .get
    var baseURL: BaseURL = .cityOfNeywork
    var endPoint: Endpoint = .scores
}
