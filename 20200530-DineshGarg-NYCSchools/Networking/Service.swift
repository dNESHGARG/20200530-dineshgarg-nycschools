//
//  FunctionalNetworking.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(NetworkError)
}

protocol Service {
    associatedtype modelObject: Decodable
    /// Ideally we can configure session in Service class only, but just keeping
    /// it settable if a request needs to initialize its own session.
    var session: URLSession { get set }
    var baseURL: BaseURL { get set }
    var endPoint: Endpoint { get set }
    var httpMethod: HTTPMethod { get set }
    
    func send(completion: @escaping (Result<modelObject>) -> Void)
}

extension Service {
    /**
     Generates a request object.

     - returns: URLRequest object.

    TODO: write implementation to add query items and params.
     
     # Notes: #
     1. An object conforming to Service protocol is required for dependencies.
     2. Consumed within send method to make network request for a session.

     # Example #
    ```
     struct SomeService: Service {
        // Add all dependencies.
     }
     
     let request = SomeService().makeRequest()
     ```
    */
    private func makeRequest() -> URLRequest? {
        
        let urlPath = "\(baseURL.rawValue)\(endPoint.rawValue)?%24limit=500&%24%24app_token=xjpXJerdelQ1oL2237Uz6SCYk"
        
        let url = URL(string: urlPath)
        
        guard let _ = url else {
            return nil
        }
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = HTTPMethod.get.rawValue
        
        return request
    }
    
    /**
     Returns a completion object on finish.

     - parameter completion: escaping closure that takes Result<T, Error> object and returns nothing.

     # Notes: #
     1. Parameters must be Result type

     # Example #
    ```
     struct SomeService: Service {
        // Add all dependencies.
     }
     
     let service = SomeService()
     service.send { result in
         switch result {
         case let .success(data):
             completion(data)
         case let .failure(error):
             completion(error)
         }
     }
     ```
    */
    func send(completion: @escaping (Result<modelObject>) -> Void) {
        guard let request = makeRequest() else {
            return
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(Result.failure(.unknownError(error)))
            }
            
            guard let response = response as? HTTPURLResponse  else {
                completion(Result.failure(.noResponse))
                return
            }
            
            if response.statusCode != 200 {
                completion(Result.failure(.invalidResponse(response.statusCode)))
            }
            
            guard let data = data else {
                completion(Result.failure(.invalidData))
                return
            }
            do {
                let schoolsData = try JSONDecoder().decode(modelObject.self, from: data)
                completion(Result.success(schoolsData))
            } catch {
                completion(Result.failure(.failureParsing(error)))
            }
        }
        task.resume()
    }
}
