//
//  SchoolDetailsViewController.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    /// By using better dependency injection tools, we can always assure that viewModel
    /// exists and is not an optional.
    var viewModel: SchoolDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 60;
    }
}

extension SchoolDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /// Can use guard to return 0 as well.
        return viewModel != nil ? viewModel!.schoolData.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDetailsCell") as! SchoolDetailsCell
        
        guard let model = viewModel else {
            return cell
        }
        
        let data = model.schoolData[indexPath.row]
        cell.setData(title: data.title, info: data.info)

        return cell
    }
}
