//
//  SchoolDetailsViewModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 6/1/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

class SchoolDetailsViewModel {
    var schoolData = [SchoolCellModel]()
    
    init(schoolData: CombinedSchoolsModel) {
        setUp(schoolData)
    }
    
    /**
     This method sets up data for the view model to be shown in School details.

     - parameter data: CombinedSchoolsModel.

     # Notes: #
     1. This is some dirty way of data formatting. A better approach would be to
     write a wrapper to iterate over struct properties.
     2. This should be improved in future.

     # Example #
    ```
     setUp(CombinedSchoolsModel())
     ```
    */
    func setUp(_ data: CombinedSchoolsModel) {
        // self.schoolData = schoolData
        guard let details = data.details else {
            return
        }
        
        schoolData.append(SchoolCellModel(title: "School Name", info: details.schoolName))
        schoolData.append(SchoolCellModel(title: "Overview", info: details.overview))
        
        if let location = details.location {
            schoolData.append(SchoolCellModel(title: "Location", info: location))
        }
        
        guard let scores = data.scores else {
            schoolData.append(SchoolCellModel(title: "SAT Scores", info: "N/A"))
            return
        }
        
        schoolData.append(SchoolCellModel(title: "Number of SAT Takers", info: scores.numSATTakers))
        schoolData.append(SchoolCellModel(title: "SAT Critical Reading Average", info: scores.satCriticalReadingAvgScore))
        schoolData.append(SchoolCellModel(title: "SAT Math Average", info: scores.satMathAvgScore))
        schoolData.append(SchoolCellModel(title: "SAT Writing Average", info: scores.satWritingAvgScore))
    }
}
