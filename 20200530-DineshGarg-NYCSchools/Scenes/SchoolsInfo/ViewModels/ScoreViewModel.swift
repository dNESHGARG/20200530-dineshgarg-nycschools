//
//  ScoreViewModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

class ScoreViewModel: ScoresProtocol {
    var scores = ScoreData()
    var worker: ScoreWorker = ScoreWorker()
}

extension ScoreViewModel {
    func fetchScores(completion: @escaping (NetworkError?) -> Void) {
        worker.fetchScore { result in
            switch result {
            case let .success(scoreData):
                self.scores = scoreData
                completion(nil)
            case let .failure(error):
                completion(error)
            }
//            self.scores = data
//            completion()
        }
    }
}

extension ScoreViewModel {
    subscript(_ index: Int) -> ScoreModel {
        get {
            return scores[index]
        }
        
        set {
            scores[index] = newValue
        }
    }
}
