//
//  CombinedViewModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

class CombinedViewModel: CombinedViewProtocol {
    var schools = CombinedSchoolData()
}

extension CombinedViewModel {
    subscript(_ index: Int) -> CombinedSchoolsModel {
        get {
            return schools[index]
        }
        
        set {
            schools[index] = newValue
        }
    }
}
