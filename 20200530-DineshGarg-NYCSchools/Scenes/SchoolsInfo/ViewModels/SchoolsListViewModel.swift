//
//  SchoolsListViewModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit

class SchoolsListViewModel {
    var schoolsList = SchoolData()
    var worker: SchoolsWorker = SchoolsWorker()
}

extension SchoolsListViewModel: SchoolsListProtocol {
    func fetchSchools(completion: @escaping (NetworkError?) -> Void) {
        worker.fetchSchools { result in
            switch result {
            case let .success(schoolData):
                self.schoolsList = schoolData
                completion(nil)
            case let .failure(error):
                completion(error)
            }
        }
    }
}

extension SchoolsListViewModel {
    subscript(_ index: Int) -> SchoolsModel {
        get {
            return schoolsList[index]
        }
        
        set {
            schoolsList[index] = newValue
        }
    }
}
