//
//  SchoolsSceneDirector.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

class SchoolsSceneDirector: SceneDirectorProtocol {
    var combinedViewModel: CombinedViewProtocol
    var schoolViewModel: SchoolsListProtocol
    var scoreViewModel: ScoresProtocol
    
    init(viewModel: CombinedViewProtocol, schoolViewModel:SchoolsListProtocol, scoreViewModel: ScoresProtocol) {
        self.combinedViewModel = viewModel
        self.schoolViewModel = schoolViewModel
        self.scoreViewModel = scoreViewModel
    }
}

extension SchoolsSceneDirector {
    func fetchSchoolsData(completion: @escaping (NetworkError?) -> Void) {
        /// Get scores and schools info and format it into CombinedViewModels.
        let group = DispatchGroup()
        
        group.enter()

        /// Ideally error would be handled in a more gracefull way.
        /// But, sending it to the caller right now as completion, so that
        /// appropriate response could be shown.
        schoolViewModel.fetchSchools { error in
            if let error = error {
                completion(error)
            }
            
            group.leave()
        }
        
        group.enter()
        
        scoreViewModel.fetchScores { error in
            if let error = error {
                completion(error)
            }
            
            group.leave()
        }
        
        group.notify(queue: DispatchQueue.global(qos: .background)) {
            self.combinedViewModel.schools =  SchoolsDataFormatter.formatSchoolsData(self.schoolViewModel.schoolsList, self.scoreViewModel.scores)
            completion(nil)
        }
    }
}
