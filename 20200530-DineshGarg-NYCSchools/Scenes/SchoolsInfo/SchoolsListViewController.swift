//
//  SchoolsListViewController.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/29/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit

protocol DirectorClient {
    func setDirector(_ director: SceneDirectorProtocol)
}

class SchoolsListViewController: UIViewController {
    var director: SceneDirectorProtocol?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var selectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 60;
        /// Since, the fetching of schools doesn't take any user action.
        /// we can safely call the fetch method once the view has loaded.
        fetchSchools()
    }
}

extension SchoolsListViewController: DirectorClient {
    /// Dependency injection via Storyboards isn't that great, should use some
    /// dependency injection library. But, it work's.
    func setDirector(_ director: SceneDirectorProtocol) {
        self.director = director
    }
}

extension SchoolsListViewController {
    /// Can also use delegate/protocol for callbacks, but closures keep it simple.
    func fetchSchools() {
        guard let director = director else { return }
        
        startLoading()
        director.fetchSchoolsData { error in
            if let error = error {
                self.failedWithError(error)
                return
            }
            
            DispatchQueue.main.async{
                self.finishedLoading()
            }
        }
    }
}

extension SchoolsListViewController: Loadable {
    func startLoading() {
        spinner.startAnimating()
        tableView.isHidden = true
    }
    
    func finishedLoading() {
        self.spinner.stopAnimating()
        self.tableView.reloadData()
        self.tableView.isHidden = false
    }
    
    func failedWithError(_ error: Error) {
        self.spinner.stopAnimating()
        tableView.isHidden = true
      
        /// Might chose some better approach if time is not a constraint.
        let alertController = UIAlertController(title: "Error loading data", message: error.localizedDescription, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Retry", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.fetchSchools()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel the alert")
        }

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// - Mark -

extension SchoolsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  director != nil ? director!.combinedViewModel.schools.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolsListCell") as! SchoolsListCell
        
        guard let director = director else { return cell }
        
        let data =  director.combinedViewModel.schools[indexPath.row]
        cell.setData(data.details)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "schoolDetails", sender: self)
    }
}

extension SchoolsListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SchoolDetailsViewController,
            let index = selectedIndex, let director = director {
            let schoolModel = director.combinedViewModel.schools[index]
            let viewModel = SchoolDetailsViewModel(schoolData: schoolModel)
            destination.viewModel = viewModel
            selectedIndex = nil
        }
    }
}
