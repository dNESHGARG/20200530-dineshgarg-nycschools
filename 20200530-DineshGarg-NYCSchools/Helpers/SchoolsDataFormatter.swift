//
//  SchoolsDataFormatter.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct SchoolsDataFormatter {
    /**
     This method formats school data and scores data to create combined school data.

     - parameter schoolsData: SchoolData where SchoolData = [SchoolsModel]
     - parameter scoresData: ScoreData where ScoreData = [ScoreModel]
     - returns: CombinedSchoolData where CombinedSchoolData = [CombinedSchoolsModel].

     # Example #
    ```
     let combined = SchoolsDataFormatter.formatSchoolsData(SchoolData(), ScoreData())
     ```
    */
    
    static func formatSchoolsData(_ schoolsData: SchoolData, _ scoresData: ScoreData) -> CombinedSchoolData {
        let schools =  schoolsData.reduce(into: [String: SchoolsModel]()) { dbn, value in
            dbn[value.dbn] = value
        }
        
        let scores =  scoresData.reduce(into: [String: ScoreModel]()) { dbn, value in
            dbn[value.dbn] = value
        }
        
        let combined: [CombinedSchoolsModel] = Array(schools.values.map {
            let scores = scores[$0.dbn]
            return CombinedSchoolsModel(details: $0, scores: scores)
            
        })
        
        return combined
    }
}

/**
 This method reads json file and returns a model.

 TODO - Should be made generic function


 - parameter fileName: String.
 - returns: SchoolData where SchoolData = [SchoolsModel].

 # Example #
```
 let data = readJSONFromFile(fileName: "file_name")
 ```
*/

func readJSONFromFile(fileName: String) -> SchoolData {
    var jsonObj = SchoolData()
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let fileUrl = URL(fileURLWithPath: path)
            let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let decoder = JSONDecoder()
            jsonObj = try decoder.decode(SchoolData.self, from: data)
            print(jsonObj)
        } catch {
            print(error)
        }
    }
    return jsonObj
}
