//
//  Mocks.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

extension ScoreModel: Mockable {
    static func mock() -> ScoreModel {
        return ScoreModel(dbn:"01M292",
                          schoolName:"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
                          numSATTakers:"29",
                          satCriticalReadingAvgScore:"355",
                          satMathAvgScore:"404",
                          satWritingAvgScore: "363")
    }
}

extension SchoolsModel: Mockable {
    static func mock() -> SchoolsModel {
        let data = readJSONFromFile(fileName: "SchoolsData")
        if (data.count > 0) { return data[0] }
        return data[0]
    }
}

extension CombinedSchoolsModel: Mockable {
    static func mock() -> CombinedSchoolsModel {
        return CombinedSchoolsModel(details: SchoolsModel.mock(), scores: ScoreModel.mock())
    }
}
