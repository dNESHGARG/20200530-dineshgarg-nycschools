//
//  Extensions.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 6/1/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

extension URL {
    /// Returns a new URL by adding the query items, or nil if the URL doesn't support it.
    /// URL must conform to RFC 3986.
    func appending(_ queryItems: [URLQueryItem]) -> URL? {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else { return nil }
        
        // append the query items to the existing ones
        urlComponents.queryItems = (urlComponents.queryItems ?? []) + queryItems

        // return the url from new url components
        return urlComponents.url
    }
}

extension URLComponents {
    init(scheme: String = "https",
         host: String,
         path: String,
         queryItems: [URLQueryItem]) {
        self.init()
        self.scheme = scheme
        self.host = host
        self.path = path
        self.queryItems = queryItems
    }
}
