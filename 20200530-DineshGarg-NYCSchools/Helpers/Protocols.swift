//
//  Protocols.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

protocol Mockable {
    /**
      Method to be implemented by any object conforming to Mockable.
     
      This methods returns an object of type Self and used for tests.
      
      # Example #
      ```
      struct SomeModel {
        let name: String
      }
      
      extension SomeModel: Mockable {
         func mock() -> SomeModel {
            return SomeModel(name: "SomeModel")
         }
      }
      ```
      */
    static func mock() -> Self
}

protocol SceneDirectorProtocol {
    var combinedViewModel: CombinedViewProtocol { get set }
    var schoolViewModel: SchoolsListProtocol { get set }
    var scoreViewModel: ScoresProtocol { get set }
    
    /**
     Method to be implemented by any object conforming to SceneDirectorProtocol.
     
     - parameter completion: An escaping closure that takes no parameters and return nothing.
     
     # Notes: #
     1. Completion handler just signifies that object has finished it's job and,
     results are ready to be used.
     
     # Example #
     ```
     struct SomeDirector: SceneDirectorProtocol {
      // Add all dependencies
     }
     
     let director = SomeDirector()
     director.fetchSchoolsData {
      // Do some job.
     }
     ```
     */
    func fetchSchoolsData(completion: @escaping (NetworkError?) -> Void)
}

protocol ScoresProtocol {
    var scores: ScoreData { get set }
    var worker: ScoreWorker { get set }
    
    /**
     Method to be implemented by any object conforming to ScoresProtocol.
     
     - parameter completion: An escaping closure that takes no parameters and return nothing.
     
     # Notes: #
     1. Completion handler just signifies that object has finished it's job and,
     results are ready to be used.
     
     # Example #
     ```
     struct ViewModel: ScoresProtocol {
     // Add all dependencies
     }
     
     let viewModel = ViewModel()
     viewModel.fetchScores {
     // Do some job.
     }
     ```
     */
    func fetchScores(completion: @escaping (NetworkError?) -> Void)
}

protocol SchoolsListProtocol {
    var schoolsList: SchoolData { get set }
    var worker: SchoolsWorker { get set }
    
    /**
     Method to be implemented by any object conforming to SchoolsListProtocol.
     
     - parameter completion: An escaping closure that takes no parameters and return nothing.
     
     # Notes: #
     1. Completion handler just signifies that object has finished it's job and,
     results are ready to be used.
     
     # Example #
     ```
     struct ViewModel: SchoolsListProtocol {
     // Add all dependencies
     }
     
     let viewModel = ViewModel()
     viewModel.fetchSchools {
     // Do some job.
     }
     ```
     */
    func fetchSchools(completion: @escaping (NetworkError?) -> Void)
}

protocol CombinedViewProtocol {
    var schools: CombinedSchoolData { get set }
}

/// Managing different states, better choice would be to use an enum for
/// states like loaded, loading, failure and previous. But, this also works fine.
protocol Loadable {
    func startLoading()
    func finishedLoading()
    func failedWithError(_ error: Error)
}
