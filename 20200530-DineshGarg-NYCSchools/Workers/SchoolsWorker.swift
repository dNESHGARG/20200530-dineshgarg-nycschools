//
//  SchoolsWorker.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit

protocol SchoolsWorkerProtocol {
    func fetchSchools(completion: @escaping (Result<SchoolData>) -> Void)
}

struct SchoolsWorker: SchoolsWorkerProtocol {
    let service = SchoolListService()
    func fetchSchools(completion: @escaping (Result<SchoolData>) -> Void) {
        service.send { result in
            switch result {
            case let .success(schoolsData):
                completion(Result.success(schoolsData))
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}
