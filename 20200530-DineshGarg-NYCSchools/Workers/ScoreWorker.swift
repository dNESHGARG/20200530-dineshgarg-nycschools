//
//  ScoreWorker.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

protocol ScoreWorkerProtocol {
    func fetchScore(completion: @escaping (Result<ScoreData>) -> Void)
}

struct ScoreWorker: ScoreWorkerProtocol {
    let service = SchoolScoreService()
    func fetchScore(completion: @escaping (Result<ScoreData>) -> Void) {
        service.send { result in
            switch result {
            case let .success(scoreData):
                completion(Result.success(scoreData))
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}
