//
//  ScoreModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

typealias ScoreData = [ScoreModel]

/**
 Score model struct.
 
 Contains SAT scores for a school.

 # Notes: #
 1. Model should conform to Codable
 2. An array of ScoreModel is being used as (typealias) ScoreData.
*/

struct ScoreModel: Decodable {
    var dbn, schoolName: String
    var numSATTakers, satCriticalReadingAvgScore,
    satMathAvgScore, satWritingAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numSATTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
