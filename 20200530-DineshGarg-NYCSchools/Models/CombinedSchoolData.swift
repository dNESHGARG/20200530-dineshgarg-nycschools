//
//  CombinedSchoolData.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/31/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

typealias CombinedSchoolData = [CombinedSchoolsModel]

/**
 Combined schools model struct.
 
 Contains school details and SAT scores for a school.

 # Notes: #
 1. An array of CombinedSchoolsModel is being used as (typealias) CombinedSchoolData.
*/

struct CombinedSchoolsModel {
    let details: SchoolsModel?
    let scores: ScoreModel?
}

