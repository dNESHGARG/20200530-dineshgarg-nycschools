//
//  SchoolsModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/30/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit

typealias SchoolData = [SchoolsModel]

/**
 Schools model struct.
 
 # Notes: #
 1. Model should conform to Codable
 2. Only few of the keys are being used. Unused keys could be discarded.
 3. An array of SchoolModels is being used as (typealias) SchoolData.
*/

struct SchoolsModel: Codable {
    let  dbn, schoolName, boro, overview: String
    
    let schoolAccessibilityDescription, academicopportunities1
    ,academicopportunities2, requirement1, requirement2, requirement3, requirement4,
    requirement5, offerRate1, admissionspriority11
    ,admissionspriority21, admissionspriority31, school10thSeats, schoolSports,
    directions1, ellPrograms, neighborhood, buildingCode, location
    ,phoneNumber, faxNumber, schoolEmail, website, subway, bus, grades2018
    ,finalgrades, totalStudents, extracurricularActivities
    ,attendanceRate, pctVariety, pctSafe
    , program1, code1, interest1, method1
    ,seats9ge1, grade9gefilledflag1, grade9geapplicants1, seats9swd1
    ,grade9swdfilledflag1, grade9swdapplicants1, seats101, grade9geapplicantsperseat1
    ,grade9swdapplicantsperseat1, primaryAddressLine, city, zip, stateCode
    ,latitude, longitude, communityBoard, councilDistrict, censusTract, bin
    ,bbl, nta, borough: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overview = "overview_paragraph"
        case school10thSeats =  "school_10th_seats"
        case academicopportunities1
        case academicopportunities2
        case ellPrograms =  "ell_programs"
        case neighborhood
        case buildingCode = "building_code"
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case grades2018
        case finalgrades
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case pctVariety = "pct_stu_enough_variety"
        case pctSafe = "pct_stu_safe"
        case schoolAccessibilityDescription = "school_accessibility_description"
        case directions1
        case requirement1 = "requirement1_1"
        case requirement2 = "requirement2_1"
        case requirement3 = "requirement3_1"
        case requirement4 = "requirement4_1"
        case requirement5 = "requirement5_1"
        case offerRate1 = "offer_rate1"
        case program1
        case code1
        case interest1
        case method1
        case seats9ge1
        case grade9gefilledflag1
        case grade9geapplicants1
        case seats9swd1
        case grade9swdfilledflag1
        case grade9swdapplicants1
        case seats101
        case admissionspriority11
        case admissionspriority21
        case admissionspriority31
        case grade9geapplicantsperseat1
        case grade9swdapplicantsperseat1
        case primaryAddressLine = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case censusTract = "census_tract"
        case bin
        case bbl
        case nta
        case borough
    }
}
