//
//  SchoolCellModel.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 6/1/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

/**
 School cell model to be shown as details.
 
 # Notes: #
 1. Constructed locally from SchoolModel and ScoreModel.
 2. Contains title which is model object name (formatted) and info that is model
 object value.
*/

struct SchoolCellModel: Codable {
    var title: String
    var info: String
}
