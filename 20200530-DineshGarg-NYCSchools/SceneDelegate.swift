//
//  SceneDelegate.swift
//  20200530-DineshGarg-NYCSchools
//
//  Created by Dinesh Garg on 5/29/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        if let firstViewController = window?.rootViewController as? UINavigationController,
            let controller = firstViewController.viewControllers[0]  as? SchoolsListViewController{
            /// Inject dependency to Initial view.
            let viewModel = CombinedViewModel()
            let schoolViewModel = SchoolsListViewModel()
            let scoreViewModel = ScoreViewModel()
            
            let director = SchoolsSceneDirector(viewModel: viewModel, schoolViewModel:schoolViewModel, scoreViewModel: scoreViewModel)
            controller.setDirector(director)
        }
        
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {}
    func sceneDidBecomeActive(_ scene: UIScene) {}
    func sceneWillResignActive(_ scene: UIScene) {}
    func sceneWillEnterForeground(_ scene: UIScene) {}
    func sceneDidEnterBackground(_ scene: UIScene) {}
}

